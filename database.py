import datetime
from peewee import *
import sys

from constants import (
  DATABASE_NAME,
  DATABASE_USER,
  DATABASE_PASSWORD,
  INIT_SQL,
)
import cli


try:
  db = MySQLDatabase(DATABASE_NAME, user=DATABASE_USER, password=DATABASE_PASSWORD)
except OperationalError:
  cli.output('Failed to connect to Database, Run the Following SQL Statements: {}'.format(INIT_SQL))  # Helper SQL to get code to run
  sys.exit()


class User(Model):
  _id = AutoField(primary_key=True)
  username = CharField(unique=True, null=False, max_length=25)
  password = CharField(null=False, max_length=25)
  email = CharField(null=False, max_length=35)
  personal_website = CharField(max_length=75)

  class Meta:
    database = db


class Post(Model):
  _id = AutoField(primary_key=True)
  created_by = ForeignKeyField(User, backref='posts')  # Link Posts to Users
  created_at = TimestampField(null=False, default=datetime.datetime.now, index=True)
  title = CharField(null=False)
  slug = CharField(unique=True)  # Web URL Slug this-is-a-post
  body = TextField(null=False)
  published = BooleanField(default=False, null=False, index=True)  # Allow for Drafts with a simple flag
  likes = IntegerField()

  class Meta:
    database = db


class Comment(Model):
  _id = AutoField(primary_key=True)
  post_id = ForeignKeyField(Post, backref='posted_on')  # Link Comments to Posts
  created_by = ForeignKeyField(User, backref='user')  # Link Users to Comments
  created_at = TimestampField(null=False, default=datetime.datetime.now)
  needs_review = BooleanField(null=False, default=True)  # Require content moderation by default
  likes = IntegerField()

  class Meta:
    database = db
