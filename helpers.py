from argparse import ArgumentTypeError
import csv
from itertools import permutations
import json
from pathlib import Path
import requests

from constants import (
  TODO_ENDPOINT,
  FILE_HEADERS,
  REQUEST_HEADERS,
  USER_ID,
  TITLE,
  BODY,
)
import cli


def _validate_response(response):

  if response.status_code in (200, 201):
    cli.output_success('{r.request.method} Response to {r.url} returned {r.status_code}'.format(r=response))
    return json.loads(response.content)

  elif response.status_code != 200:
    cli.output_error('{r.request.method} response returned to {r.url}: {r.status_code}'.format(r=response))
    return


def _validate_output_directory(path):
  path = Path(path)

  if not path.is_dir():  # Verify the Path is actually a directory
    raise ArgumentTypeError('Invalid Output Directory Path, please pass a path to a directory')

  return path.resolve()


def _validate_input_path(path):
  path = Path(path)

  if not path.is_file():
    raise ArgumentTypeError('Invalid Output Directory Path, please pass a valid file path')

  return path


def _write_to_csv(output_directory, data):
  complete_path = '{}/todos.csv'.format(output_directory)

  with open(complete_path, 'w+') as f:
    writer = csv.DictWriter(f, fieldnames=FILE_HEADERS)
    writer.writeheader()
    writer.writerows(data)

  cli.output_success('Data written to {}'.format(complete_path))


def force_valid_user_selection(prompt):
  valid_id = None

  while not valid_id:
    valid_id = cli.get_numeric_user_input(prompt)

    if not valid_id:  # 0 is a not considered a valid value
      cli.cls()
      cli.output_error('Invalid ID Supplied, please enter a valid integer value')

  return valid_id


def get_todos():
  response = requests.get(TODO_ENDPOINT)
  todos = _validate_response(response)

  if not todos:
    return

  cli.output_success('Found {} TODOs'.format(len(todos)))
  return todos


def delete_todo(valid_id):
  delete_todo_endpoint = '{}/{id}'.format(TODO_ENDPOINT, id=valid_id)
  response = _validate_response(requests.delete(delete_todo_endpoint))


def create_todo():
  user_id = force_valid_user_selection('Enter your User ID: ')
  todo_title = input('Enter the TODO title: ')
  todo_content = input('Enter the body of your TODO: ')

  todo_obj = json.dumps({
    USER_ID: user_id,
    TITLE: todo_title,
    BODY: todo_content
  })

  response = requests.post(TODO_ENDPOINT, data=todo_obj, headers=REQUEST_HEADERS)
  response_payload = _validate_response(response)

  if not response_payload:
    return

  cli.output_success('New TODO Created w/ ID: {r[id]}'.format(r=response_payload))
  return response_payload


def get_permutations(other):
  results = []
  strings = permutations(other)
  results = map(''.join, strings)
  return ','.join(sorted(results))
