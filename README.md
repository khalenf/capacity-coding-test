
# Python Version: **3.7.4**

Make sure a version of the python3 binary is on your path!

## Setting up the Environment

To install the required packages for this project run the following commands:


1. Optional: `python3 -m venv ~/path/to/new/virtual-environment`
2. Optional: `source ~/path/to/new/virtual-environment/bin/activate`
3. `pip install -r requirements.txt`
## API Interactions Script:

Run the script with the following command on a UNIX System:

- `./api_interactions.py -m get` - Retrieves list of 200 TODO Objects from the API


- `./api_interactions.py -m delete` - Deletes a TODO Object based on the user selected ID during script runtime


- `./api_interactions.py -m create` - Creates a TODO object based on user input. outputs to script source directory by default


- `./api_interactions.py -o /home/ -m create` - Output directory flag, user must pass the path to an existing folder


## Algorithms:

Run the script with the following commands:


- `./algorithms.py /path/to/input.txt`
    - Where /path/to/input.txt is the path to an input file containing strings separated by a newline character


## Database Design:

The Database schema has been designed using ORM peewee, it should represent a basic
schema for a modern blog application. This script is not meant to be run necessarily but can
interfaced with interactively using a python shell.
Make sure you have some mysql Drivers installed:

https://github.com/PyMySQL/mysqlclient-python
`sudo apt-get install python-dev default-libmysqlclient-dev # Debian / Ubuntu`

`sudo yum install python-devel mysql-devel # Red Hat / CentOS`

`brew install mysql-connector-c # macOS (Homebrew)`


## Note:
Please update your .env file with the appropriate values for the Database User, Database Password, and Database Name
