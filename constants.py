import os

from dotenv import load_dotenv

load_dotenv(verbose=True)

GET = 'get'
CREATE = 'create'
DELETE = 'delete'
RUNTIME_MODES = [GET, CREATE, DELETE]

USER_ID = 'userId'
ID = 'id'
TITLE = 'title'
BODY = 'body'
COMPLETED = 'completed'

FILE_HEADERS = [USER_ID, ID, TITLE, COMPLETED]

BASE_API_URL = 'http://jsonplaceholder.typicode.com'
TODO_ENDPOINT = '{}/todos'.format(BASE_API_URL)

REQUEST_HEADERS = {'Content-Type': 'application/json; charset=UTF-8'}

DATABASE_USER = os.getenv('DATABASE_USER')
DATABASE_PASSWORD = os.getenv('DATABASE_PASSWORD')
DATABASE_NAME = os.getenv('DATABASE_NAME')

INIT_SQL = '''
CREATE USER IF NOT EXISTS '{user}'@'localhost'
  IDENTIFIED WITH sha256_password BY '{passw}'
  PASSWORD EXPIRE INTERVAL 180 DAY;

CREATE DATABASE IF NOT EXISTS {database};

GRANT ALL PRIVILEGES ON {database}.* TO '{user}'@'localhost';
'''.format(
  user=DATABASE_USER,
  passw=DATABASE_PASSWORD,
  database=DATABASE_NAME,
)
