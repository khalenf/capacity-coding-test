#!/usr/bin/env python3
"""
Write a script which prints all the permutations of a string in alphabetical order. We consider that digits < upper
case letters < lower case letters. The sorting should be performed in ascending order.
"""
import argparse
from itertools import permutations
from pathlib import Path  # Using pathlib for portability

import cli
from helpers import (
  _validate_input_path,
  get_permutations,
)


def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('input_file', type=_validate_input_path, help='File containing strings to print permutations of')

  args = parser.parse_args()

  with args.input_file.open() as f:  # pathlib.Path syntax for file opening
    input_data = [x for x in f.read().split('\n') if x]

  cli.output_success('Found {} lines of input data'.format(len(input_data)))
  permutations = map(get_permutations, input_data)

  cli.output('\n'.join(permutations))


if __name__ == '__main__':
  main()
