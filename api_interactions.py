#!/usr/bin/env python3
import argparse
from pathlib import Path

import cli
from constants import (
  RUNTIME_MODES,
  GET,
  CREATE,
  DELETE,
)
from helpers import (
  _validate_output_directory,
  _write_to_csv,
  force_valid_user_selection,
  get_todos,
  delete_todo,
  create_todo,
)


def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('-m', '--mode', choices=RUNTIME_MODES, help='Select the Run Mode')
  parser.add_argument('-o', '--output-directory', type=_validate_output_directory, default=Path('.'), help='Path to the output directory')

  args = parser.parse_args()

  if args.mode == GET:
    todos = get_todos()

    if not todos:
      cli.output_error('Failed to return TODO data from API...')
      return

    _write_to_csv(args.output_directory, todos)  # Opted to write data to CSV for easily reviewable output, understanding its not part of the exercise

  elif args.mode == DELETE:
      valid_id = force_valid_user_selection('Enter the ID of the TODO you would like to Delete: ')
      result = delete_todo(valid_id)

  elif args.mode == CREATE:
    create_todo()


if __name__ == '__main__':
  main()
