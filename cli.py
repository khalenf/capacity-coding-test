class colors:
  OKGREEN = '\033[92m'
  WARNING = '\033[93m'
  FAIL = '\033[91m'
  ENDC = '\033[0m'


def cls():
  print("\n" * 50)


def output(t):
  print(t)


def output_warning(t):
  print('{c.WARNING}{t}{c.ENDC}'.format(c=colors, t=t))


def output_success(t):
  print('{c.OKGREEN}{t}{c.ENDC}'.format(c=colors, t=t))


def output_error(t):
  print('{c.FAIL}{t}{c.ENDC}'.format(c=colors, t=t))


def get_numeric_user_input(prompt):
  response = input(prompt)
  return int(response) if response.isdigit() else None
